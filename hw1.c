#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//limit of string
#define N 256


//method of sort
int int_sort(const void* a, const void* b){
	return strcmp((char*)a,(char*)b);
}


//return : 1 -> ok, 0　->　no
int compareWord(char *s, char *word){
	int i;
	char c=*word;
	//compare each word 
	for(i=0; i<strlen(s); i++){
		if(s[i]==c){
			c=*(++word);
		}
		if(c=='\0')
			return 1;
	}
	return 0;
}


//
void searchWord(char *s){
	int i, maxN=0;
	char ans[N]="\0";
	char dictWord[N]="\0";
	char targetWord[N]; strcpy(targetWord, s);
	qsort((void*)s, strlen(s), sizeof(s[0]), int_sort);
	

    FILE *fp;
    char *filename = "american-english";
    char readline[N] = {'\0'};

    //file open
    if ((fp = fopen(filename, "r")) == NULL) {
        fprintf(stderr, "Open error in %s.\n", filename);
        exit(EXIT_FAILURE);
    }


    //read and compare
    while ( fgets(readline, N, fp) != NULL ) {
    		readline[strlen(readline)-1]='\0';
    		if(strcmp(readline, targetWord)==0) continue;
     	
     	strcpy(dictWord, readline);
     	qsort((void*)dictWord, strlen(dictWord), 
			sizeof(dictWord[0]), int_sort);

     	if(compareWord(s, dictWord)==1 && 
			strlen(dictWord)>maxN){
			maxN=strlen(readline);
			strcpy(ans, readline);
		}
		
    }


    fclose(fp);


	printf("------------------------------\n\n");
	printf(" Anagram is  %s  ( %d words ) \n\n", ans, maxN);

}



int main(void){
	char s[N];//target string
	int i;

	//input stream
	printf("please input the word\n");
	if(fgets(s, N, stdin)==NULL){
		fprintf(stderr, "there is error.");
		exit(1);
	}

	if(strchr(s, '\n')!=NULL){
		s[strlen(s)-1]='\0';//delete "\n"
	}else {
		fprintf(stderr, "over the number of string");
		exit(1);
	}

	printf("\nYour WORD is %s\n", s);


	searchWord(s);



}