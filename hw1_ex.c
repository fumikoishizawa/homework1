#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//limit of string
#define N 256
#define maxArray 100000

char dictArray[maxArray][N];
//once outputed word
char wordReserved[10][N];
int count=0;


//method of sort
int int_sort(const void* a, const void* b){
	return strcmp((char*)a,(char*)b);
}


void input(char *s, int num){
	if(fgets(s, num, stdin)==NULL){
		fprintf(stderr, "there is error.");
		exit(1);
	}

	if(strchr(s, '\n')!=NULL){
		s[strlen(s)-1]='\0';//delete "\n"
	}else {
		fprintf(stderr, "over the number of string");
		exit(1);
	}

}

//search resercedword
//return 1->ok 0->no
int ReservedWord(char *word){
	int i;
	for(i=0; i<10; i++){
		if(strcmp(word, wordReserved[i])==0) return 1;
	}
	return 0;
}


//return : 1 -> ok, 0　->　no
int compareWord(char *s, char *word){
	int i;
	char c=tolower(*word);
	//compare each word 
	for(i=0; i<strlen(s); i++){
		s[i]=tolower(s[i]);
		if(s[i]==c){
			c=tolower(*(++word));
		}
		if(c=='\0')
			return 1;
	}
	return 0;
}


//
void searchWord(char *s){
	int i, maxN=0;
	char ans[N]="\0";
	char dictWord[N]="\0";
	char targetWord[N];
	

	strcpy(targetWord, s);
	qsort((void*)s, strlen(s), sizeof(s[0]), int_sort);

    //read and compare
     for(i=0; i<maxArray; i++) {
    		if(strcmp(dictArray[i], targetWord)==0 || 
    			ReservedWord(dictArray[i])==1) 
    			continue;

     	
     	strcpy(dictWord, dictArray[i]);
     	qsort((void*)dictWord, strlen(dictWord), 
			sizeof(dictWord[0]), int_sort);
     	if(compareWord(s, dictWord)==1 && 
			strlen(dictWord)>maxN){
			maxN=strlen(dictArray[i]);
			strcpy(ans, dictArray[i]);
		}
		
    }

    if(strcmp(ans, "\0")==0)	strcpy(ans, "not exit");
    

    strcpy(wordReserved[count++], ans);

	printf("------------------------------\n\n");
	printf(" Anagram is  %s  ( %d letters ) \n\n", ans, maxN);

}



int main(void){
	FILE *fp; int i=0;
    char *filename = "american-english";
    char readline[N] = {'\0'};
    char s[N];//target string
    char scape[2];

     //file open
    if ((fp = fopen(filename, "r")) == NULL) {
        fprintf(stderr, "Open error in %s.\n", filename);
        exit(EXIT_FAILURE);
    }
    
	while ( fgets(readline, N, fp) != NULL ) {
    		readline[strlen(readline)-1]='\0';
    		strcpy(dictArray[i++], readline);
    	}



	printf("please input the word\n");
	input(s, N);
	printf("\nYour WORD is %s\n", s);

	for(i=0; i<10; i++){
		searchWord(s);
		printf("------------------------------\n\n");
		printf("--If you continue searching, Enter \n");
		printf("----[END] Ctrl+C\n\n");
		input(scape, 2);

	}


	fclose(fp);


}